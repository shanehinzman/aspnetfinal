﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MagnoliaBakery.App_Data;

namespace MagnoliaBakery.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Menu()
        {
            MenuController populator = new MenuController();

            populator.Load();

            return View(populator);
        }

        public ActionResult CheckExistingName(string ValidName)
        {
            bool ifEmailExist = false;
            try
            {
                ifEmailExist = ValidName.Equals("Shane") ? true : false;
                return Json(!ifEmailExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}