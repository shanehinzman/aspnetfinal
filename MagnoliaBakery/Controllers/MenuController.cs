﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MagnoliaBakery.App_Data
{
    public class MenuController : Controller
    {
        public MenuController()
        {
            Items = new List<MenuItem>();
        }

        public List<MenuItem> Items { get; set; }

        public void Load()
        {
            MenuItem entity = new MenuItem();

            entity.MenuID = 1;
            entity.MenuTitle = "I Cupcake New York";
            entity.MenuDescription = "An edible ode to our hometown! The I Cupcake NY package combines Magnolia Bakery’s favorites. Two red velvets and one each of the classics, vanilla & chocolate cake with vanilla & chocolate buttercream. All cupcakes are shipped frozen to ensure freshness.";
            entity.MenuImage = "https://pantograph0.goldbely.com/s410/uploads/product_image/image/8275/i-cupcake-new-york.fa879fb157840930ab6891f58df470f2.jpg";
            entity.MenuPrice = 30;
            Items.Add(entity);

            entity = new MenuItem();
            entity.MenuID = 2;
            entity.MenuTitle = "Baker's Choice Bars Assortment";
            entity.MenuDescription = "A beautiful and delicious assortment of Magnolia Bakery’s double fudge brownies, chocolate chunk blondies and magic cookie bars.";
            entity.MenuImage = "https://pantograph0.goldbely.com/s410/uploads/product_image/image/8346/bakers-choice-bars-assortment.1ddd25a1f59a89a1de2d0583dab50000.jpg";
            entity.MenuPrice = 45;
            Items.Add(entity);

            entity = new MenuItem();
            entity.MenuID = 3;
            entity.MenuTitle = "Chocolate Chunk Cookie Favor Box";
            entity.MenuDescription = "Nestled inside are six of Magnolia Bakery’s classic chocolate chunk cookies. Choose between a white favor box or Magnolia branded favor box. Choose your occasion tag.";
            entity.MenuImage = "https://pantograph0.goldbely.com/s410/uploads/product/main_image/6740/chocolate-chunk-cookie-favor-box.ea21ebb4ae0291caf3c8a817c43261dc.jpg";
            entity.MenuPrice = 12;
            Items.Add(entity);

            entity = new MenuItem();
            entity.MenuID = 4;
            entity.MenuTitle = "Coconut Macaroons";
            entity.MenuDescription = "A wonderfully sweet coconut flavor with a crisp exterior and a soft, chewy interior.";
            entity.MenuImage = "https://pantograph0.goldbely.com/s410/uploads/product_image/image/13129/coconut-macaroons.998e47146ad4db3d8d82c0e402b3fcbf.JPG";
            entity.MenuPrice = 22;
            Items.Add(entity);

            entity = new MenuItem();
            entity.MenuID = 5;
            entity.MenuTitle = "Magnolia Bakery Chocolate Bars";
            entity.MenuDescription = "Enjoy our new artisanal chocolate bars, available in the following flavors: banana pudding, red velvet, PB&J, toffee bar, lemon bar and 72% dark chocolate.";
            entity.MenuImage = "https://pantograph0.goldbely.com/s410/uploads/product_image/image/8160/magnolia-bakery-chocolate-bars.ac8e6a2363c6209de3672e843757fbbd.jpg";
            entity.MenuPrice = 38;
            Items.Add(entity);
        }
    }
}