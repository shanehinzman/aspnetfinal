﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MagnoliaBakery.Models;

namespace MagnoliaBakery.Controllers
{
    public class FormController : Controller
    {
        //
        // GET: /Form/

        public ActionResult Comment()
        {
            CommentModel model = new CommentModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult SubmitInfo(CommentModel model)
        {
            TempData["model"] = model;

            return Redirect("/Form/PostedComment");
        }

        public ActionResult PostedComment()
        {
            CommentModel model = TempData["model"] as CommentModel;

            return View(model);
        }
    }
}
