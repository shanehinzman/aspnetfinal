﻿using MagnoliaBakery.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MagnoliaBakery.Models
{
    public class CommentModel
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        //validation
        //required
        [Required(ErrorMessage = "Your name is required")]
        //stringlength
        [StringLength(100, MinimumLength=1, ErrorMessage = "Name needs to be within 1 and 100 characters.")]
        //remote
        [Remote("CheckExistingName", "Home", ErrorMessage = "Name already exists!")]
        public string ValidName { get; set; }
        [Required(ErrorMessage = "Your email is required")]
        //regular expression
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email doesn't look like a valid email address.")]
        public string ValidEmail { get; set;}
        [Required(ErrorMessage = "Your age is required")]
        //range
        [Range(0,200, ErrorMessage = "Age must be a number from 0 to 200")]
        public string ValidAge { get; set; }
        [Required(ErrorMessage = "Your comment is required")]
        //custom
        [MaxWords(500, ErrorMessage = "There are too many words in {0}")]
        [StringLength(500, ErrorMessage = "Comment can only be 500 characters long")]
        public string ValidComment { get; set; }
    }
}