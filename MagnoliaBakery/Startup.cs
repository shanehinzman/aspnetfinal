﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MagnoliaBakery.Startup))]
namespace MagnoliaBakery
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
