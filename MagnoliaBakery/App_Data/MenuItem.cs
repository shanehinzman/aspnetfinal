﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MagnoliaBakery.App_Data
{
    public class MenuItem
    {
        public int MenuID { get; set; }
        public string MenuTitle { get; set; }
        public string MenuDescription { get; set; }
        public string MenuImage { get; set; }
        public double MenuPrice { get; set; }

        public override string ToString()
        {
            return MenuTitle;
        }
    }
}